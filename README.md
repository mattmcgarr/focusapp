# Focus App

# Overview

Quick focus app I built to basics of React Native.

# Functionality

This mobile app allows users to add tasks they would like to focus on, set a timer for the duration they would like to focus, and then display a list of tasks they have focused on. Additionally, the timer resets after completition, allowing users to either add more time or complete the task. When the timer ends, the phone will vibrate to notify the user. The application also keeps the phone awake while in use.
